<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />

    <title>IGN presents Heroes &amp; Villains Fantasy Team</title>


      <link rel="shortcut icon" href="favicon.ico">
      <link rel=apple-touch-icon href="apple-touch-icon.png">
      <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
      <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">

      <meta property="og:locale" content="en_GB" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="IGN presents Heroes &amp; Villains Fantasy Team" />
      <meta property="og:type" content="product" />
      <meta property="og:url" content="http://uk-microsites.ign.com/heroes-and-villains" />
      <meta property="og:image" content="http://uk-microsites.ign.com/heroes-and-villains/images/social.jpg" />
      <meta property="og:site_name" content="IGN presents Heroes &amp; Villains Fantasy Team" />
      <meta property="og:description" content="Pick your Heroes &amp; Villains Fantasy Team from the choices below and let your friends know who's on your side." />
      <meta name="twitter:card" content="summary_large_image"/>
      <meta name="twitter:description" content="Pick your Heroes &amp; Villains Fantasy Team from the choices below and let your friends know who's on your side."/>
      <meta name="twitter:title" content="IGN presents Heroes &amp; Villains Fantasy Team "/>
      <meta name="twitter:domain" content="IGN"/>
      <meta name="twitter:image:src" content="http://uk-microsites.ign.com/heroes-and-villains/images/social.jpg"/>
      <meta name="twitter:creator" content="@ignuk"/>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="css/app.css" />
      <script src="bower_components/modernizr/modernizr.js"></script>
      <script src="http://geobeacon.ign.com/geodetect.js" type="text/javascript"></script>
  </head>
  <body id="clickable" style="cursor: pointer;" onClick="_gaq.push(['_trackEvent', 'click', 'skin'])">
     <script>
       window.fbAsyncInit = function() {
         FB.init({
           appId      : '1698407740393894',
           xfbml      : true,
           version    : 'v2.5'
         });
       };

       (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>


     <!--uncomment for live-->
      <div id="policyNotice" style="display:none">
        <div class="close-btn">
            <a href="#_" title="Close" data-domain=".ign.com">Close</a>
        </div>
        <p>We use cookies and other technologies to improve your online experience. By using this Site, you consent to this use as described in our <a href="http://corp.ign.com/policies/cookie-eu" target="_blank">Cookie Policy</a>.</p>
      </div>
      <!--uncomment for live-->


<!--off canvas menu-->
 <div class="off-canvas-wrap" data-offcanvas>
   <div class="inner-wrap">
     <nav class="tab-bar top" data-topbar>

         <section class="left-small show-for-small-only">
             <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
         </section>

         <div class="row">

           <div class="ign_red-wrapper ">
             <h1 class="title"><a href="http://ign.com" id="home_hook"><img src="images/ign-logo.png" alt="IGN logo"></a></h1>
           </div>

           <!-- <ul class="left share_btn" id="share_override">
             <li><a href="#"></a></li>
           </ul> -->

           <div class="addthis_sharing_toolbox"></div>


         </div>

     </nav>

     <aside class="left-off-canvas-menu show-for-small-only">
      <ul class="off-canvas-list">
         <li><label></label></li>
             <li><a href=""></a></li>
             <li><a href=""></a></li>
             <li><a href=""></a></li>
      </ul>
     </aside>
     <!--end off canvas menu-->

     <!--content-->
     <section class="main-section">

        <!--start banner-->
        <div class="banner">
          <div class="banner__container">

             <div id="ifame-container" style="display: none;">
                <iframe style="height: 66px; overflow: hidden; width: 970px; border: none;" src="./halo-banner/index.html" class="aim_us"></iframe>
             </div>

             <div class="row" id="jsBannerHook" style="display: none;">
                <div class="large-6 medium-6 columns">
                   <img src="images/banner2.png" alt="">
                </div>
                <div class="large-6 medium-6 columns">
                   <div class="youtube youtube__container">
                      <div class="flex-video widescreen youtube">
                        <!-- <div class="youtube__wrapper"> -->
                           <div id="player"></div>
                        <!-- </div> -->
                      </div>
                   </div>
                </div>
             </div>
          </div>
        </div>
        <!--end banner-->

        <div class="row site__container" id="jsSiteHook" style="cursor: default;">

            <!--top panel title-->

            <div class="toppanel toppanel__container">
               <div class="toppanel__copyleft">
                  <h1><img class="show" src="images/heading.jpg" alt=""></h1>

                  <img src="images/heading.jpg">
                  <!--share icon-->
                  <div class="share" id="jsShareHook"><a href="#_"><i class="fa fa-share"></i></a></div>
                  <!--end share icon-->
               </div>



               <div class="toppanel__copyright">
                  <p class="heavyitalic hide-for-small">If you were about to duke it out with your arch
<br>rival, who would you want covering your back?</p>
                  <p class="heavyitalic show-for-small-only">If you were about to duke it out with your arch rival, who would you want covering your back?</p>

<p>Would you always go for the good guy, or whoever's the strongest, even if they're evil? Pick your Fantasy Team from the choices below and let your friends know who's on your side. <span id="geoText">Plus, enter your details for the chance to win the ultimate <span class="heavyitalic">Halo 5: Guardians prize bundle!</span></span></p>
               </div>
            </div>

            <!--end top panel title-->



             <!--START vote panel 1-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax1" class="parallaxParent">
               	 <div style="background-image: url(./images/bg/bg_1.jpg)"></div>
                </div>
                <!--end parallax bg-->

                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">

                   <img src="images/characters/luke_blk.png" alt="">
                   <img src="images/characters/luke.png" alt="">

                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/darth_vader_blk.png" alt="">
                   <img src="images/characters/darth_vader.png" alt="">
                </div>

                <div class="votepanel--title">
                   <ul>
                     <li><span>Luke</span> Skywalker</li>
                     <li>vs</li>
                     <li><span>Darth</span> Vader</li>
                   </ul>
                </div>

                <div class="votepanel__select">


                   <div class="votepanel__select--checkbox good" data-name="skyWalker">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="vader">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 2-->
             <div class="votepanel votepanel__container">


                <!--parallax bg-->
                <div id="parallax2" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_2.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/mario_blk.png" alt="">
                   <img src="images/characters/mario.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/bowser_blk.png" alt="">
                   <img src="images/characters/bowser.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>mario</li>
                    <li>vs</li>
                    <li>bowser</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="mario">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="bowser">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 3-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax3" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_3.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/sonic_blk.png" alt="">
                   <img src="images/characters/sonic.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/eggman_blk.png" alt="">
                   <img src="images/characters/eggman.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>sonic</li>
                    <li>vs</li>
                    <li>eggman</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="sonic">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="eggman">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 4-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax4" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_4.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/batman_blk.png" alt="">
                   <img src="images/characters/batman.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/joker_blk.png" alt="">
                   <img src="images/characters/joker.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>batman</li>
                    <li>vs</li>
                    <li>joker</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="batman">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="joker">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 5-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax5" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_5.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/thor_blk.png" alt="">
                   <img src="images/characters/thor.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/loki_blk.png" alt="">
                   <img src="images/characters/loki.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>thor</li>
                    <li>vs</li>
                    <li>loki</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="thor">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="loki">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 6-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax6" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_6.jpg)"></div>
                </div>
                <!--end parallax bg-->

                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/spiderman_blk.png" alt="">
                   <img src="images/characters/spiderman.png" alt="">
                </div>

                <div class="votepanel__character last large">
                   <img src="images/characters/goblin_blk.png" alt="">
                   <img src="images/characters/goblin.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>spiderman</li>
                    <li>vs</li>
                    <li><span>green</span> goblin</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="spiderman">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="goblin">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->




             <!--START vote panel 7-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax7" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_7.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/cloud_blk.png" alt="">
                   <img src="images/characters/cloud.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/seph_blk.png" alt="">
                   <img src="images/characters/seph.png" alt="">
                </div>


                <div class="votepanel--title">
                  <ul>
                    <li>cloud</li>
                    <li>vs</li>
                    <li>sephiroth</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="cloud">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="sephiroth">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->




             <!--START vote panel 8-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax8" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_8.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/crash_blk.png" alt="">
                   <img src="images/characters/crash.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/neo_blk.png" alt="">
                   <img src="images/characters/neo.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li><span>crash</span> bandicoot</li>
                    <li>vs</li>
                    <li><span>neo</span> cortex</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="crash">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="neo">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 9-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax9" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_9.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/ripley_blk.png" alt="">
                   <img src="images/characters/ripley.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/alien_blk.png" alt="">
                   <img src="images/characters/alien.png" alt="">
                </div>


                <div class="votepanel--title">
                  <ul>
                    <li>ripley</li>
                    <li>vs</li>
                    <li>alien</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="ripley">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="alien">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 10-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax10" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_10.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/xavier_blk.png" alt="">
                   <img src="images/characters/xavier.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/magneto_blk.png" alt="">
                   <img src="images/characters/magneto.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>xavier</li>
                    <li>vs</li>
                    <li>magneto</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="xavier">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="magneto">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 11-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax11" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_11.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/gandalf_blk.png" alt="">
                   <img src="images/characters/gandalf.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/sauron_blk.png" alt="">
                   <img src="images/characters/sauron.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>gandalf</li>
                    <li>vs</li>
                    <li>saruman</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="gandalf">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="saruman">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 12-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax12" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_12.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/link_blk.png" alt="">
                   <img src="images/characters/link.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/ganondorf_blk.png" alt="">
                   <img src="images/characters/ganondorf.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>link</li>
                    <li>vs</li>
                    <li>ganondorf</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="link">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="ganondorf">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->




             <!--START vote panel 13-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax13" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_13.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/daredevil_blk.png" alt="">
                   <img src="images/characters/daredevil.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/kingpin_blk.png" alt="">
                   <img src="images/characters/kingpin.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>daredevil</li>
                    <li>vs</li>
                    <li>kingpin</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="daredevil">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="kingpin">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 14-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax14" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_14.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/wolverine_blk.png" alt="">
                   <img src="images/characters/wolverine.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/sabretooth_blk.png" alt="">
                   <img src="images/characters/sabretooth.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li>wolverine</li>
                    <li>vs</li>
                    <li>sabretooth</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="wolverine">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="sabretooth">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->



             <!--START vote panel 15-->
             <div class="votepanel votepanel__container">

                <!--parallax bg-->
                <div id="parallax15" class="parallaxParent">
                   <div style="background-image: url(./images/bg/bg_15.jpg)"></div>
                </div>
                <!--end parallax bg-->


                <div class="votepanel__spacer">

                </div>

                <div class="votepanel__character first">
                   <img src="images/characters/masterchief_blk.png" alt="">
                   <img src="images/characters/masterchief.png" alt="">
                </div>

                <div class="votepanel__character last">
                   <img src="images/characters/arbiter_blk.png" alt="">
                   <img src="images/characters/arbiter.png" alt="">
                </div>

                <div class="votepanel--title">
                  <ul>
                    <li><span>master</span> chief</li>
                    <li>vs</li>
                    <li>arbiter</li>
                  </ul>
              </div>


                <div class="votepanel__select">

                   <div class="votepanel__select--checkbox good" data-name="masterchief">
                      <div class="check"></div>
                   </div>

                   <div class="votepanel__select--bar">
                      <div class="goodbar"></div>
                      <div class="badbar"></div>
                   </div>

                   <div class="votepanel__select--checkbox bad" data-name="arbiter">
                      <div class="check"></div>
                   </div>

                </div>

             </div>
             <!--END vote panel-->

             <!--submit section-->
             <div class="submitbar">
                <div class="submitbar__container">
                   <div class="row">
                      <div class="large-8 columns">
                        <p>Submit your results and find out how<br> everyone else voted! </p>
                     </div>
                      <div class="large-4 columns">
                        <a href="#_" class="btn disabled" id="jsVoteSubmit">Submit</a>
                     </div>
                   </div>
                </div>
             </div>
             <!--end submit section-->

             <!--START competition panel-->
             <div class="comp comp__wrapper" id="compHook" data-equalizer>

                <div class="comp__section comp__section--good" data-equalizer-watch>

                </div>
                <div class="comp__section comp__section--bad" data-equalizer-watch>

                   <p>For a chance to win a 500GB Xbox One with Kinect, Halo 5: Guardians Limited Collector’s Edition, a Limited Edition MegaBlox Chief/Locke/Arbiter set, Limited Edition Locke Astro A40 Headset, Limited Edition Guardian Wireless Controller and a Halo 5 t-shirt, just enter your details below.</p>

                   <form method="post" class="form-horizontal" id="modal_form" data-abide="ajax">

                      <div id="ajaxLoader"><img src="images/loader01.gif" alt="loading..."></div>

                       <!-- thankyou message shown on successfull db entry :) -->
                       <div class="thankyou-modal" style="display:none;">
                         <p>Thanks for your entry!</p>
                       </div>

                       <div class="row">
                         <div class="large-6 columns">
                           <label>FIRST NAME

                             <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="first_name" value="<?php if(isset($_POST['first_name'])){echo $_POST['first_name'];}?>">
                           </label>
                           <small class="error">First name is required</small>
                         </div>
                         <div class="large-6 columns">
                           <label>LAST NAME
                             <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="last_name" value="<?php if(isset($_POST['last_name'])){echo $_POST['last_name'];}?>">
                           </label>
                           <small class="error">Last name is required</small>
                         </div>
                       </div>

                       <div class="row">
                         <div class="large-12 columns">
                           <label>EMAIL
                             <input type="email" class="forminput" required name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">
                           </label>
                           <small class="error">Email is required</small>
                         </div>
                       </div>

                       <div class="row">
                         <div class="large-12 columns">
                           <div class="row pushDown">
                     <!--                        <div class="large-1 column">
                               <input id="checkbox1" type="checkbox" name="terms" value="1">
                             </div>
                             <div class="large-11 column">
                               <label class="checkboxLabel" for="checkbox1">I confirm that I am a UK resident over the age of 18 and have read and understood the <a href="#_">terms and conditions and the privacy policy.</a></label>
                             </div> -->
                           </div>

                             <div class="row">
                               <div class="large-1 medium-1 small-1 column">
                                  <input id="checkbox2" type="checkbox" name="terms" value="1">
                               </div>
                               <div class="large-11 medium-11 small-11 column">
                                  <label class="checkboxLabel" required for="checkbox2">I confirm I am a UK resident over the age of 18 and I have fully read and understood the <a href="http://uk.ign.com/articles/2014/05/14/uk-competition-terms-and-conditions" target="_blank" style="color: #e8e8e8; text-decoration: underline;">terms and conditions.</a></label>
                               </div>
                             </div>
                           </div>
                         </div>

                         <div class="row">
                           <div class="large-12 columns">
                             <input type="hidden" name="_captcha" value="">
                             <input type="hidden" name="_form" value="modal">
                             <input type="submit" value="SUBMIT" class="submit_btn btn" name="_submit">
                           </div>
                         </div>
                       <!-- </div> -->
                     </form>

                </div>

             </div>
             <!--END competition panel-->


        </div>


        <!--start ign footer-->
         <footer class="tab-bar">
            <div class="row">

               <div class="ign_red-wrapper">
                 <div class="title"><img src="images/ziff-davis-logo.png" alt="Ziff Davis Logo"></div>

                 <ul>
                   <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                   <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
                 </ul>
               </div>

               <ul class="left links">
                  <li>
                    <ul>
                      <li><p>Copyright 2015<br>Ziff Davis International Ltd.</p></li>
                      <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                      <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                      <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
                    </ul>
                  </li>
               </ul>

            </div>
           </footer>
           <!--end ign footer-->
           <!-- Place the tag where you want the button to render -->


     </section>
     <!--end content section-->

     <a class="exit-off-canvas"></a>

     </div>
   </div>




   <!-- START modal -->
   <div class="ui basic modal" id="jsShareModal">
     <i class="close icon"></i>
     <div class="row">
        <div class="large-4 columns">
           <span class="share-title">Facebook</span>
           <a id="facebookQuoteBtn" class="fb_btn share s_facebook"><i class="fa fa-facebook"></i><span id="facebookQuoteCounter" class="fb_counter badge counter c_facebook"></span></a>
        </div>
        <div class="large-4 columns">
           <span class="share-title">Twitter</span>
           <a id="twitterQuoteBtn" class="tw_btn share s_twitter"><i class="fa fa-twitter"></i><span id="twitterQuoteCounter" class="tw_counter badge counter c_twitter"></span></a>
        </div>
        <div class="large-4 columns">
           <span class="share-title">Google+</span>
           <a id="plusQuoteBtn" class="ps_btn share s_plus g-interactivepost"
           data-contenturl="http://uk-microsites.ign.com/heroes-and-villains/"
           data-contentdeeplinkid=""
           data-clientid="810447656620-6euo8dd159vi2me5th5hjmngsm71ckrq.apps.googleusercontent.com"
           data-cookiepolicy="single_host_origin"
           data-prefilltext="If you were about to duke it out with your arch rival, who would you want covering your back?"
           data-calltoactionlabel="CREATE"
           data-calltoactionurl="http://uk-microsites.ign.com/heroes-and-villains/"
           data-calltoactiondeeplinkid=""><i class="fa fa-google-plus"></i><span id="plusQuoteCounter" class="ps_counter badge counter c_plus"></span></a>
        </div>
     </div>
  </div>
  <!--end share modal-->


   <!-- START modal -->
   <div class="ui small modal" id="jsResultsModal">
     <i class="close icon"></i>

     <div class="header">
       <span class="thin">Your Team is</span> <span id="jsPrecentageMain">0%</span>
     </div>

     <div class="typeheader">
        <span class="name"></span>

        <p id="additionalCopy"></p>
        <p><span class="bold">Share your result, and check out below to<br> see how everyone voted!</span></p>
     </div>

     <div class="share_panel">

        <div class="row">
          <div class="large-4 columns">

             <span class="share-title"><a id="facebookListBtn" class="fb2_btn share s_facebook"><i class="fa fa-facebook"></i>Facebook</a></span>

          </div>
          <div class="large-4 columns">

             <span class="share-title"><a id="twitterListBtn" class="tw2_btn share s_twitter"><i class="fa fa-twitter"></i>Twitter</a></span>

          </div>
          <div class="large-4 columns">

             <span class="share-title"><a id="plusListBtn" class="ps2_btn share s_plus g-interactivepost"><i class="fa fa-google-plus"></i>Google+</a></span>

          </div>
        </div>

     </div>

     <div class="results__container content vertical-images" id="jsVotepanelHook">

        <!--START votepanel bar 1-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/luke.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Luke Skywalker</span>
             <span class="votepanel__rightname">Darth Vader</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>


           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/darth_vader.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 1-->

        <!--START votepanel bar 2-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/mario.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Mario</span>
             <span class="votepanel__rightname">Bowser</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

              <span class="votepanel__leftpercentage">0%</span>
              <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/bowser.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 2-->

        <!--START votepanel bar 3-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/sonic.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Sonic</span>
             <span class="votepanel__rightname">Eggman</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/eggman.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 3-->


        <!--START votepanel bar 4-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/batman.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Batman</span>
             <span class="votepanel__rightname">Joker</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>


           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/joker.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 4-->


        <!--START votepanel bar 5-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/thor.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">

             <span class="votepanel__leftname">Thor</span>
             <span class="votepanel__rightname">Loki</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/loki.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 5-->



        <!--START votepanel bar 6-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/spiderman.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Spider-Man</span>
             <span class="votepanel__rightname">Green Goblin</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/goblin.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 6-->



        <!--START votepanel bar 7-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/cloud.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Cloud</span>
             <span class="votepanel__rightname">Sephiroth</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/seph.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 7-->



        <!--START votepanel bar 8-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/crash.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Crash Bandicoot</span>
             <span class="votepanel__rightname">Neo Cortex</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/neo.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 8-->


        <!--START votepanel bar 9-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/ripley.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Ripley</span>
             <span class="votepanel__rightname">Alien</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/alien.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 9-->


        <!--START votepanel bar 10-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/xavier.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Xavier</span>
             <span class="votepanel__rightname">Magneto</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/magneto.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 10-->


        <!--START votepanel bar 11-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/gandalf.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Gandalf</span>
             <span class="votepanel__rightname">Saruman</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/sauron.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 11-->


        <!--START votepanel bar 12-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/link.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Link</span>
             <span class="votepanel__rightname">Gananondorf</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/ganondorf.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 12-->


        <!--START votepanel bar 13-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/daredevil.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Daredevil</span>
             <span class="votepanel__rightname">Kingpin</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/kingpin.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 13-->


        <!--START votepanel bar 14-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/wolverine.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Wolverine</span>
             <span class="votepanel__rightname">Sabretooth</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/sabretooth.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 14-->


        <!--START votepanel bar 15-->
        <div class="votepanel__select">

           <div class="votepanel__select--checkbox good">
             <img src="images/characters/masterchief.png" alt="">
           </div>

           <div class="votepanel__select--bar" data-good="" data-bad="">
             <span class="votepanel__leftname">Master Chief</span>
             <span class="votepanel__rightname">Arbiter</span>

             <div class="goodbar"></div>
             <div class="badbar"></div>

             <span class="votepanel__leftpercentage">0%</span>
             <span class="votepanel__rightpercentage">0%</span>
           </div>

           <div class="votepanel__select--checkbox bad">
             <img src="images/characters/arbiter.png" alt="">
           </div>

        </div>
        <!--END votepanel bar 15-->


     </div>

   </div>
   <!--END modal-->


   <div id="jsResultsList">
      <span class="skywalker" data-pair="1"></span>
      <span class="vader" data-pair="1"></span>
      <span class="mario" data-pair="2"></span>
      <span class="bowser" data-pair="2"></span>
      <span class="sonic" data-pair="3"></span>
      <span class="eggman" data-pair="3"></span>
      <span class="batman" data-pair="4"></span>
      <span class="joker" data-pair="4"></span>
      <span class="thor" data-pair="5"></span>
      <span class="loki" data-pair="5"></span>
      <span class="spiderman" data-pair="6"></span>
      <span class="goblin" data-pair="6"></span>
      <span class="cloud" data-pair="7"></span>
      <span class="sephiroth" data-pair="7"></span>
      <span class="crash" data-pair="8"></span>
      <span class="neo" data-pair="8"></span>
      <span class="ripley" data-pair="9"></span>
      <span class="alien" data-pair="9"></span>
      <span class="xavier" data-pair="10"></span>
      <span class="magneto" data-pair="10"></span>
      <span class="gandalf" data-pair="11"></span>
      <span class="saruman" data-pair="11"></span>
      <span class="link" data-pair="12"></span>
      <span class="ganondorf" data-pair="12"></span>
      <span class="daredevil" data-pair="13"></span>
      <span class="kingpin" data-pair="13"></span>
      <span class="wolverine" data-pair="14"></span>
      <span class="sabretooth" data-pair="14"></span>
      <span class="masterchief" data-pair="15"></span>
      <span class="arbiter" data-pair="15"></span>
   </div>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="node_modules/semantic-ui-transition/transition.min.js"></script>
    <script src="node_modules/semantic-ui-dimmer/dimmer.min.js"></script>
    <script src="node_modules/semantic-ui-modal/modal.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.js"></script>
    <script src="js/SocialShare.min.js"></script>
    <script src="js/slimscroll.js"></script>

    <!--scrollmagic-->
    <script src="node_modules/gsap/src/uncompressed/TweenMax.js"></script>
    <script src="node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js"></script>
    <script src="node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
    <script src="node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
    <!--end scrollmagic-->


    <script src="//cdn.jsdelivr.net/velocity/1.2.3/velocity.min.js"></script>
    <script src="js/app.js"></script>

    <script>
     // 2. This code loads the IFrame Player API code asynchronously.
     var tag = document.createElement('script');

     tag.src = "https://www.youtube.com/iframe_api";
     var firstScriptTag = document.getElementsByTagName('script')[0];
     firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

     // 3. This function creates an <iframe> (and YouTube player)
     //    after the API code downloads.
     var player;
     function onYouTubeIframeAPIReady() {
       player = new YT.Player('player', {
         height: '390',
         width: '640',
         videoId: 'piH8tpQClp8',
         events: {
           'onReady': onPlayerReady,
           'onStateChange': onPlayerStateChange
         }
       });
     }

     // 4. The API will call this function when the video player is ready.
     function onPlayerReady(event) {
       event.target.playVideo();
       event.target.mute();
     }

     // 5. The API calls this function when the player's state changes.
     //    The function indicates that when playing a video (state=1),
     //    the player should play for six seconds and then stop.
     var done = false;
     function onPlayerStateChange(event) {
      //  if (event.data == YT.PlayerState.PLAYING && !done) {
      //    setTimeout(stopVideo, 6000);
      //    done = true;
      //  }
     }
   //   function stopVideo() {
   //     player.stopVideo();
   //   }
   </script>


    <script type="text/javascript">

        (function( jQuery ) {
        if ( window.XDomainRequest ) {
          jQuery.ajaxTransport(function( s ) {
            if ( s.crossDomain && s.async ) {
              if ( s.timeout ) {
                s.xdrTimeout = s.timeout;
                delete s.timeout;
              }
              var xdr;
              return {
                send: function( _, complete ) {
                  function callback( status, statusText, responses, responseHeaders ) {
                    xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
                    xdr = undefined;
                    complete( status, statusText, responses, responseHeaders );
                  }
                  xdr = new window.XDomainRequest();
                  xdr.onload = function() {
                    callback( 200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType );
                  };
                  xdr.onerror = function() {
                    callback( 404, "Not Found" );
                  };
                  xdr.onprogress = function() {};
                  if ( s.xdrTimeout ) {
                    xdr.ontimeout = function() {
                      callback( 0, "timeout" );
                    };
                    xdr.timeout = s.xdrTimeout;
                  }

                  xdr.open( s.type, s.url, true );
                  xdr.send( ( s.hasContent && s.data ) || null );
                },
                abort: function() {
                  if ( xdr ) {
                    xdr.onerror = jQuery.noop();
                    xdr.abort();
                  }
                }
              };
            }
          });
        }
      })( jQuery );

  </script>

   <script type="text/javascript">

   /* Voting api */
    $(document).ready(function(){

      function populateModal(){

         for (var k=1 ; k<=15 ; k++ ) {

            $('#jsResultsList').find('[data-pair="'+ k +'"]').each(function(e){
                values[e] = parseInt($(this).text());
             });

             $.each(values,function() {
                  total += this;
             });

             c1 = Math.round((100/total)*values[0]);
             c2 = Math.round((100/total)*values[1]);

             if((c1+c2)!==100) {
                c1 = c1-1;
             }

             //empty vars...
             values = [];
             total = 0;

             $('#jsResultsModal').find('.votepanel__select--bar').eq(k-1).attr('data-good', c1).attr('data-bad', c2);

         }
      }

      var _ignapi = {
        'url': 'http://ec2-52-18-186-21.eu-west-1.compute.amazonaws.com/vote.php?_=' + Math.random()
      };

      var values = [],
          total = 0,
          c1 = 0,
          c2 = 0;



      /* Load votes on launch */
      $.ajax({
        url: _ignapi.url,
        type: 'GET',
        data: 'all',
        cache: 'false'
      }).success(function(msg){
        res = $.parseJSON(msg);
        for(var fighter in res) {

          $('#jsResultsList > span.' + fighter).text(res[fighter]);

        }

      });

      /* Register votes silently */
      $('#jsVoteSubmit').on('click', function(){

        $('.votepanel__select--checkbox.active').each(function(){


          var ajaxType = "POST";

         //  if ($.browser.msie) {
         //    ajaxType = "GET";
         //  }

          //console.log($(this).attr('data-name'));

          $.ajax({
            url: _ignapi.url,
            type: ajaxType,
            cache: false,
            data: { "winner" : $(this).attr('data-name') } ,
            error: function(jqXHR, textStatus, errorThrown) {
            }
          }).success(function(msg){
             populateModal();
          });

        });
     });
    });


    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'ultimate-battle_crew']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    <!-- Google Analytics -->
   //   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   //   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   //   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   //   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
     //
   //   ga('create', 'UA-15279170-1', 'ultimate-battle_crew');  // Replace with your property ID.
   //   ga('send', 'pageview');

     <!-- End Google Analytics -->


    //clickable skins
    $(function(){

      $('#clickable').on('click', function(){

         if($(this).hasClass('uk-skin')) {
            var win = window.open('http://www.xbox.com/en-gb/games/halo-5-guardians', '_blank');
            win.focus();
         } else {
            var win = window.open('http://www.xbox.com/en-US/games/halo-5-guardians', '_blank');
            win.focus();
         }

      });

      $('#jsSiteHook').on('click', function(e){
          e.stopPropagation();
      });

      $('#clickable').on('click', '#jsResultsModal', function(e){
          e.stopPropagation();
      });

      $('#clickable').on('click', '.dimmer', function(e){
          e.stopPropagation();
      });

      $('#home_hook').on('click', function(e){
          e.stopPropagation();
      });

    });

</script>
   <!--end GA script-->

   <!-- Go to www.addthis.com/dashboard to customize your tools -->
   <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56166a556b5e72cf" async="async"></script>

  </body>
</html>
