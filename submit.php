<?php

/**
 * Harkable Competition Submission
 *
 * To create a competition entry you simply need to POST the following data to this script:
 *
 * first_name : String  : The user's first name
 * last_name  : String  : The user's last name
 * email      : String  : The user's email address
 * terms      : Integer : 1 if terms accepted
 */

// Turn off error reporting
error_reporting(0);

// Include the Hk library
include_once('./lib/HkCompetition.class.php');

// Instantiate the competition object
$comp = new HkCompetition('ign_halo');

// Has a form been posted to this script?
if ($comp->form_posted())
{
  // Set entry data
  $comp->set_data(array(
    'first_name' => $comp->post('first_name'),
    'last_name' => $comp->post('last_name'),
    'email' => $comp->post('email'),
    'terms' => $comp->post('terms')));

  // If data was successfully submitted
  if ($comp->submit())
  {
    echo json_encode(array(
      'success' => TRUE));
  }

  // If there was an error
  else {
    echo json_encode(array(
      'success' => FALSE,
      'error' => "Sorry, the form couldn't be submitted."));
  }
}

// If no formdata posted
else {
  echo json_encode(array(
    'success' => FALSE,
    'error' => "Direct script access not allowed"));
}
