$(document).foundation();


$(function(){

    function UrlArray (n) {
      this.length = n;
      for (var i =1; i <= n; i++) {
        this[i] = ' '
      }
    }
   url = new UrlArray(7);
   url[0] = './halo-banner/index.html';
   url[1] = './halo-banner-2/index.html';
   url[2] = './halo-banner-3/index.html';
   url[3] = './halo-banner/index.html';
   url[4] = './halo-banner-2/index.html';
   url[5] = './halo-banner-3/index.html';
   url[6] = './halo-banner/index.html';
   var currentdate = new Date();
   var urlnumber = currentdate.getDay();
   $('#ifame-container').find('iframe').attr('src', url[urlnumber]);

   if (window. _ZDGEOCOUNTRY == 'GB') {
      $('#compHook').show();
      $('#geoText').show();
   }
   else {
      $('#compHook').hide();
      $('#geoText').hide();
   }

   if (window. _ZDGEOCOUNTRY == 'GB') {

      $('#jsBannerHook').show();
      $('#ifame-container').hide();
      $('#clickable').addClass('uk-skin');

   } else {

      $('#ifame-container').show().parent().css('height', '66px');
      $('#clickable').addClass('us-skin');
      $('#jsBannerHook').hide();

   }


   //removing tap delay
   $.event.special.tap = {
     setup: function() {
       var self = this,
         $self = $(self);

       // Bind touch start
       $self.on('touchstart', function(startEvent) {
         // Save the target element of the start event
         var target = startEvent.target;

         // When a touch starts, bind a touch end handler exactly once,
         $self.one('touchend', function(endEvent) {
           // When the touch end event fires, check if the target of the
           // touch end is the same as the target of the start, and if
           // so, fire a click.
           if (target == endEvent.target) {
             $.event.simulate('tap', self, endEvent);
           }
         });
       });
     }
   };

   $('#modal_form').on('valid.fndtn.abide', function() {
      // disable submit button
      $('#modal_form .submit_btn').prop('disabled', true);

      $('#ajaxLoader').fadeIn(200);
      // save form data
      saveForm('#modal_form');
   });

   var saveForm = function(formType){

	        //var data = $( formType ).serialize();

           var formData = {
               'first_name' : $('input[name=first_name]').val(),
               'last_name' : $('input[name=last_name]').val(),
               'email' : $('input[name=email]').val(),
               'terms' : $("input:checkbox").is(":checked") ? 1:0
           };

           console.log(formData);

	        $.ajax({
	            url: "submit.php",
	            type: "POST",
	            data: formData,

	            /**
	             * Function called if the request succeeds
	             * @method success
	             * @param {Object} d
	             */
	            success: function(response, textStatus, jqXHR) {

                  if(response && JSON.parse(response).success){

                     $('#ajaxLoader').fadeOut(200);

	            		// success saving form data
	            		$( formType+" .forminput" ).val("");
	            		$( formType+" .thankyou-modal" ).fadeIn(1000);
	            		$( formType+" .row").animate({
	            			opacity : 0.6
	            		}, 300, 'swing');

	            	} else {
	            		// error saving form data
	            		alert('There was an error saving your data, please try again shortly.');

                     $('#ajaxLoader').fadeOut(200);

	            	}

	            	$(formType+' .submit_btn').prop('disabled', false);

	            },
	            /**
	             * Function called if the request failed
	             * @method error
	             * @param {jqXHR} jqXHR
	             * @param {String} status
	             * @param {String} errorThrown
	             */
	            error: function(jqXHR, status, errorThrown) {

	            	alert('There was an error saving your data, please try again shortly.');
	            	$(formType+' .submit_btn').prop('disabled', false);

	            }

	        });

	} // end save function


   $('#jsShareHook').on('click', 'a', function(){

      $('#jsShareModal').modal({
         onShow    : function(){
            //do something after showing share modal
         }
      }).modal('show');

   });


   //-------- share buttons


   $('.fb_btn').on('click', function(){

      FB.ui({
        method: 'feed',
        link: 'http://uk-microsites.ign.com/heroes-and-villains',
        caption: 'If you were about to duke it out with your arch rival, who would you want covering your back?',
      }, function(response){

      });

   });



   $('.fb_counter').ShareCounter({
      url: 'http://uk-microsites.ign.com/heroes-and-villains'
   });

   $('.tw_btn').ShareLink({
      title: 'IGN presents Heroes & Villains Fantasy Team',
      text: 'If you were about to duke it out with your arch rival, who would you want covering your back?',
      image: 'http://uk-microsites.ign.com/heroes-and-villains/images/social.jpg',
      url: 'http://uk-microsites.ign.com/heroes-and-villains'
   });


   $('.tw_counter').ShareCounter({
      url: 'http://uk-microsites.ign.com/heroes-and-villains'
   });

   // $('.ps_btn').ShareLink({
   //    title: 'IGN presents Ultimate Battle Crew',
   //    text: 'If you were about to duke it out with your arch rival, who would you want covering your back?',
   //    image: '',
   //    url: 'http://192.168.33.10/'
   // });


   $('.ps_counter').ShareCounter({
      url: 'http://uk-microsites.ign.com/heroes-and-villains'
   });




   function setupShares(e, p, d){

      var characters = e.toString(),
            p = Math.round(p);

      $('.fb2_btn').on('click', function(){

         FB.ui({
           method: 'feed',
           link: 'http://uk-microsites.ign.com/heroes-and-villains',
           caption: 'Here’s who I’d want in my Heroes & Villains Fantasy Team - ' + characters + ' - which side would you pick?',
         }, function(response){
            console.log(response);
         });

      });


      $('.tw2_btn').ShareLink({
         title: 'IGN presents Heroes & Villains Fantasy Team ',
         text: 'My Heroes & Villains Fantasy Team is '+ p +'% '+ d +'! Whats yours?',
         image: 'http://uk-microsites.ign.com/heroes-and-villains/images/social.jpg',
         url: 'http://uk-microsites.ign.com/heroes-and-villains'
      });

      //$('#plusListBtn').attr('data-prefilltext', 'Here’s who I’d want in my Battle Crew - ' + characters + ' - which side would you pick?');

      var options = {
        contenturl: 'http://uk-microsites.ign.com/heroes-and-villains',
        contentdeeplinkid: '',
        clientid: '810447656620-6euo8dd159vi2me5th5hjmngsm71ckrq.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        prefilltext: 'Here’s who I’d want in my Heroes & Villains Fantasy Team - ' + characters + ' - which side would you pick?',
        calltoactionlabel: 'CREATE',
        calltoactionurl: 'http://uk-microsites.ign.com/heroes-and-villains',
        calltoactiondeeplinkid: ''
     };


     // Call the render method when appropriate within your app to display
     // the button.
     gapi.interactivepost.render('plusListBtn', options);

      // $('.ps2_btn').ShareLink({
      //    title: 'IGN presents Ultimate Battle Crew',
      //    text: 'Here’s who I’d want in my Battle Crew - ' + characters + ' - which side would you pick?',
      //    image: '',
      //    url: 'http://192.168.33.10/'
      // });

   }

   //-------- end share buttons


   //shift size of bars when voting...
   function percentageBar(d){

      var $e = d.parent();

      if(d.hasClass('good')) {

         $e.find('.goodbar').velocity({
            width: '120px'
         }, 300);

         $e.find('.badbar').velocity({
            width: '0px'
         }, 300);

      } else {

         $e.find('.badbar').velocity({
            width: '120px'
         }, 300);

         $e.find('.goodbar').velocity({
            width: '0px'
         }, 300);

      }

   }


   //shift bars and animate percentage
   function animateResult(d){

      d.find('.votepanel__select--bar').each(function(){

         var $e = $(this),
             gval = Math.round($e.attr('data-good')),
             bval = Math.round($e.attr('data-bad'));

             $e.find('.badbar').velocity({
                width: bval + '%'
             }, 1500, 'easeOutSine');

             $e.find('.goodbar').velocity({
                width: gval + '%'
             }, 1500, 'easeOutSine');


             function doGvalTimeout(i){
                setTimeout(function(){
                   $e.find('.votepanel__leftpercentage').text(i + '%');
                }, i*20);
             }

             function doBvalTimeout(i){
                setTimeout(function(){
                   $e.find('.votepanel__rightpercentage').text(i + '%');
                }, i*20);
             }


             for(var i=1; i<=gval; i++) {
                doGvalTimeout(i);
             }

             for(var i=1; i<=bval; i++) {
                doBvalTimeout(i);
             }


      });

   }


   //vote array
   var voteList = [];
   var charList = [];
   var c = '';

   //on clicking vote button, check state...
   $('#jsSiteHook').on('click', '.votepanel__select--checkbox', function(){

      var $this = $(this),
          bool = $this.parent().find('.active').length;

      //darken images
      if($this.hasClass('good')){

         if($this.parents('.votepanel').find('.faded').length) {
            $this.parents('.votepanel').find('.faded').velocity({
               opacity : 1
            }, 300, 'easeOutSine', function(){
               $(this).removeClass('faded');
            });
         }

         $this.parents('.votepanel').find('.last > img').eq(1).velocity({
            opacity : 0.2
         }, 300, 'easeOutSine', function(){
            $(this).addClass('faded');
         });

      } else {

         if($this.parents('.votepanel').find('.faded').length) {
            $this.parents('.votepanel').find('.faded').velocity({
               opacity : 1
            }, 300, 'easeOutSine', function(){
               $(this).removeClass('faded');
            });
         }

         $this.parents('.votepanel').find('.first > img').eq(1).velocity({
            opacity : 0.2
         }, 300, 'easeOutSine', function(){
            $(this).addClass('faded');
         });

      }

      //if active add to array...
      if(bool == 1) {
         $this.parent().find('.active').removeClass('active');
      } else if(bool == 0) {
         voteList.push(1);
      }

      //make the section active...
      $this.addClass('active');

      //animate percentage bar before modal...
      percentageBar($this);

      //count actives and active submit button
      if(voteList.length == 15) {
         $('#jsVoteSubmit').removeClass('disabled');
      }

   });


   function doLrgTimeout(i){
      setTimeout(function(){
         $('#jsPrecentageMain').text(i + '%');
      }, i*30);
   }

   var n = 0,
      lrg = 0;

   $('#jsVoteSubmit').on('click', function(){

      //if voting complete...
      if(voteList.length == 15) {

         $('.active').each(function(){

            c = $(this).attr('data-name');
            charList.push(c);

            //number of good characters...
            if($(this).hasClass('good')) {
               n++;
            }

         });

         //calculate percentage...
         var p = n/15*100,
            gore = '';


         if($('#jsResultsModal').hasClass('activated') !== true) { //if it hasn't been activated before

            //change modal header dependant on whether its good or evil
            if(p >= 50) {

               $('#jsResultsModal').find('.name').text('Good').parent().addClass('good');
               gore = 'Good';

               if(p<60) {
                  $('#additionalCopy').text('You’ve got a pretty good balance of good and evil in your team. Be careful though, the good guys may still get tempted by the forces of evil. Just keep Luke away from Joker, he can be a very bad influence.');
               } else if(p<80 && p>=60) {
                  $('#additionalCopy').text('Most of your guys have a heart of gold, but there are one or two who aren’t quite on the straight and narrow.');
               } else if(p>=80) {
                  $('#additionalCopy').text('Congratulations, your team is a flock of angels! No evil shall penetrate your squad and your heroes will back you up at a moment’s notice, all with a smile on their face.');
               }


            } else if (p < 50) {

               p = 100-p;
               $('#jsResultsModal').find('.name').text('Evil').parent().addClass('evil');
               gore = "Evil";

               if(p<60) {
                  $('#additionalCopy').text('You’ve got a pretty good balance of good and evil in your team. Be careful though, the good guys may still get tempted by the forces of evil. Just keep Luke away from Joker, he can be a very bad influence.');
               } else if(p<80 && p>=60) {
                  $('#additionalCopy').text('You’ve got a horrible bunch of Villains in your team, but just enough Heroes to bring them down a peg or two when they’re being too evil.');
               } else if(p>=80) {
                  $('#additionalCopy').text('Talk about pure evil. Your team has a black heart and no morals, but there’s no question they’re a bunch of badasses. Good on your for giving in and embracing the dark side!');
               }

            }

         }

         //reveal the modal...
         $('#jsResultsModal').modal({
            onShow    : function(){

               if($('#jsResultsModal').hasClass('activated') !== true) { //if it hasn't been activated before

                  $('#jsResultsModal').addClass('activated');

                  $('#jsVotepanelHook').find('.votepanel__select').each(function(e){

                     var d = 150*e;

                     $(this).delay(d).velocity({
                        opacity : 1,
                        marginTop: '0px'
                     }, 200, 'easeOutSine', function(){

                           if(e == 7) { //once 7 revealed, show...
                              animateResult($('#jsVotepanelHook'));
                           }

                     });

                     for(var i=0; i<p; i++) {
                        doLrgTimeout(i);
                     }

                  });

                  setupShares(charList, p, gore);

               }

            }
         }).modal('show');

      }

   });


});



(function($){

   // var ua = navigator.userAgent;
   // function is_touch_device() {
   //      try {
   //          document.createEvent("TouchEvent");
   //          return true;
   //      } catch (e) {
   //          return false;
   //      }
   // }
   //
   // if ((is_touch_device()) || ua.match(/(iPhone|iPod|iPad)/)
   // || ua.match(/BlackBerry/) || ua.match(/Android/)) {} else {

   var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));

   if(isTouch !== 1) {

      $(window).load(function(){

         $("#jsVotepanelHook").mCustomScrollbar({
             theme:"light"
         });

      });

   }
})(jQuery);


// var ua = navigator.userAgent;
// function is_touch_device() {
//      try {
//          document.createEvent("TouchEvent");
//          return true;
//      } catch (e) {
//          return false;
//      }
// }
//
//
//
// if ((is_touch_device()) || ua.match(/(iPhone|iPod|iPad)/)
// || ua.match(/BlackBerry/) || ua.match(/Android/)) {} else {

var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));

if(isTouch !== 1) {

   //parallax containers...
   // init controller
   var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "500%"}});

   var scenes = [];

   for (var i = 0; i < $('.parallaxParent').length; ++i) {
       scenes[i] = 'parallax_' + i;
   }

   // build scenes
   $('.parallaxParent').each(function(e){

      var id = $(this).attr('id');

      scenes[e] = new ScrollMagic.Scene({triggerElement: '#'+id, offset: 0})
      				.setTween('#'+id+" > div", {y: "60%", ease: Linear.easeNone})
      				.addTo(controller);


   });

}

//}
